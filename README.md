This is an experimental proxy to serve geminimaps to conventional web browsers.  
for instance, you could pull up the same .gmi page in html at `http(s)://example.com/index.gmi` and in a gemini client at `gemini://example.com/index.gmi`, if you also have a gemini server such as [gemserv](https://codeberg.org/Valenoern/gemserv) running.

currently the code needs some cleanup (possibly debugging),  
but has already been tested out on [extend.monster](http://extend.monster/descriptions).

Where its own license is applicable, this proxy is under AGPL3.  
(The footer in the default template should be an example of what sufficient source code attribution looks like.)


## configuration

To use this proxy you should copy or symlink this folder into your site root, and make sure it has the correct owners and permissions to be found by nginx.  
(there is probably a better way to do this but that will be fixed in a future version.)

You should also configure nginx with a server block that will serve your gemini files, and make sure all your fastcgi settings are correct (which is sometimes difficult, and could be its own full page).  
A sample configuration looks something like this:

```
server {
        listen 80;
        listen [::]:80;

        server_name extend.test localhost;
        root /path/to/root/;

        index index.html index.htm index.gmi index.nginx-debian.html;

        location / {
                # First attempt to serve request as file, then
                # as directory, then fall back to displaying a 404.
                try_files $uri $uri/ =404;
        }

        # redirect server error pages to the static page /50x.html
        error_page   500 502 503 504  /50x.html;
        #error_page  404              /404.html;
        location = /50x.html {
                root   /usr/share/nginx/html;
        }

        include /etc/nginx/conf.d/geminimaps.conf;
        include /etc/nginx/conf.d/php.conf;
}

```

In the `geminimaps.conf` location block you can specify a custom page template.

(Note that even though it would make the most _sense_, I haven't yet tested this with https.)

## files

* `proxy.php` and `main.js` contain proxy code.
* `geminimaps.conf` and `php.conf` contain configuration rules to include in an nginx `server {}` block.<br /> `php.conf` is likely unnecessary if you can already run php; you can see an nginx tutorial for more detailled instructions on nginx .conf files and how to use and troubleshoot them
* `index.html`, `test.php`, and `test.gmi` are for testing that nginx and php are working.
