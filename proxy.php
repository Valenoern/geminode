<?php
$repodir = dirname(__FILE__);
//$_GET["geminode"];
$gmifile = $_GET["page"];
$root = $_GET["root"];
$template = $_GET["template"];

/*
print("<pre>");
exec("pwd", $output);
$pwd = join("",$output);

print("gmi file: $repodir/main.js \"$root$gmifile\" \n");
print("pwd: $pwd \n");
print("root: $root \n");
print("</pre>");
*/

// try to prevent a 'bobby tables' attack from running random commands
// https://xkcd.com/327/
// i couldn't actually manage to run commands through the query string but yeah
$gmipath = escapeshellcmd("$root$gmifile");

exec("/usr/bin/node $repodir/main.js \"$gmipath\" \"$template\"", $output);
print(join("\n",$output));
?>
