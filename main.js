/*
  transform a .gmi file into simple html
  usage:
  node /path/to/main.js map.gmi template.html
  html template is optional, if omitted the result will be unstyled
*/

var fs = require('fs');
// regexes to test lines for each gemtext thing
const patterns = linePatterns(), defaultTempName = './template.html';
// get map filename from the first argument
var gmiMapName = process.argv[2], templateName = process.argv[3], template;

loadTemplate(templateName);


function loadTemplate(templateName) {
	var defaultTemplate = false;

	// if template not supplied use default
	if (templateName === undefined) {
		templateName = defaultTempName;
		defaultTemplate = true;
	}

	fs.readFile(templateName, 'utf8', (err, data) => {
		if (err) {
		if (defaultTemplate === false) {
			loadTemplate(defaultTempName);
		}
		// if default template can't be found just return the text
		else {
			template = '${BODY}';
			parseGmiMap();
		}
	}
	// if template loaded, continue
	else {
		template = data;
		parseGmiMap();
	}
	});
}

function parseGmiMap() {
// read file from supplied filename & parse
fs.readFile(gmiMapName, 'utf8', (err, data) => {
	if (err) throw err;
	var i = 0, j, linkLoc, linkText, quoteOn = false, listOn = false, preOn = false, blockMode = false, result = [];

	var parsed = data.split('\n');
	
	const stopBlocksIfOn = function() {
		if (preOn === true) {
			blockMode = true;
			
			if (patterns.pre.test(parsed[i]) === true) {
				preOn = false;
				// replace line rather than adding to avoid getting '```'
				result[i] = '</pre>\n</figure>';
			}
		}
		// blockquotes end when there stop being >
		else if (quoteOn === true) {
			if (patterns.quote.test(parsed[i]) === true) {
				result[i] = result[i].replace(patterns.quote, '$1');
			}
			else {
				quoteOn = false;
				//blockMode = true;
				result[i] = '</blockquote>\n' + result[i];
			}
		}
		// lists end when there stop being *
		// TODO: I'm not sure this is the best way
		// it doesn't allow spacing out list bullets for readability / separating lists
		else if (listOn === true) {
			if (patterns.list.test(parsed[i]) === true) {
				result[i] = startList(parsed[i]);
			}
			else {
				listOn = false;
				result[i] += '</ul>';
			}
		}
	};
	
	const startQuote = function(line) {
		var startTag = '';
		if (quoteOn !== true) {
			startTag = '<blockquote>\n';
			quoteOn = true;
		}
		return line.replace(patterns.quote, startTag + '$1');
	};
	
	const startList = function(line) {
		var startTag = '';
		if (listOn !== true) {
			startTag = '<ul>\n';
			listOn = true;
		}
		blockMode = true;
		return line.replace(patterns.list, startTag + '<li>$1</li>');
	};
	
	const startPre = function(line) {
		preOn = true;
		blockMode = true;
		var preText = line.replace(patterns.pre, '$1');
		return '<figure title="' + preText + '" aria-label="' +  preText + '">\n<pre>';
	};

	for (i = 0; i < parsed.length; i++) {
		result[i] = parsed[i];
		blockMode = false;
		
		// escape brackets
		result[i] = result[i].replace(/\</g, '&lt;').replace(/(.)\>/g, '$1&gt;');

		if (preOn === true || quoteOn === true || listOn === true) {
			stopBlocksIfOn();
		}
		else {
			if (patterns.link.test(parsed[i]) === true) {
				result[i] = linkLine(parsed[i], patterns);
			}
			else if (patterns.heading.test(parsed[i]) === true) {
				result[i] = headingLine(parsed[i], patterns);
				blockMode = true;
			}
			else if (patterns.quote.test(parsed[i]) === true) {
				result[i] = startQuote(parsed[i]);
			}
			else if (patterns.list.test(parsed[i]) === true) {
				result[i] = startList(parsed[i]);
			}
			else if (patterns.pre.test(parsed[i]) === true) {
				result[i] = startPre(parsed[i]);
			}
		}
		
		// add break to the end of lines
		// this will not be added after a few things like headings
		if (i < parsed.length - 1 && blockMode !== true) {
			// line breaks are considered "somewhat" semantic in gemini (at least versus html) so a tag is best
			result[i] += '<br />';
		}
	}

	// stringify result & output;
	// here console.log is actually used to pipe text to other programs
	result = linesToText(result, template);
	console.log(result);
});
}


// helpers

// process a heading, like:
// # Page title
function headingLine(line, patterns) {
	var headingLevel = line.replace(patterns.heading, '$1').length;

	// in the gemini spec there are only three levels of headings (up to ###)
	if (headingLevel > 3) {
		headingLevel = 3;
	}
			
	return line.replace(patterns.heading, '<h' + headingLevel + '>$2</h' + headingLevel + '>');
}

// process a link line, like:
// => protocol://path/to/page.ext Page name
function linkLine(line, patterns) {
	var linkText = line.replace(patterns.link, '$3');
	var linkLoc = line.replace(patterns.link, '$1');
	
	// a line might not have a page title, in which case just use its url
	if (linkText == '') {
		linkText = linkLoc;
	}
	
	return '=&gt; <a href="' + linkLoc + '">' + linkText + '</a>';
}

function linesToText(lineArray, template) {
	var joined = lineArray.join('\n');
	// temp workaround:
	// currently there are too many lines around preformatted figures
	var result = joined.replace(/\<pre\>\n\n?/g, '<pre>').replace(/\n\<\/pre\>/g, '</pre>');
	//var result = joined.replace(/\<pre\>\n\n?/g, '<pre>');
	
	// insert rendered html into supplied template
	var templated = template.replace('${BODY}', result);
	
	return templated;
}

function linePatterns() {
	return {
		heading: /^(\#+)\s?(.+)$/,
		link: /^\=\>\s*([^\s]+)(\s+(.+))?$/,
		list: /^\*\s(.+)$/,
		quote: /^\>\s*(.*)$/,
		pre: /^\`\`\`(.+)?$/
	};
}
