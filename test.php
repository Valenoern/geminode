<!doctype html>
<html>
<head><title>PHP test</title></head>
<body>


<h1>php test</h1>

This page will run the following code:

<pre>
&lt;?php 
print("PHP is working!");

// debug what directory relative links will open in
exec("pwd", $output);   
$pwd = join("",$output);
print("current directory: $pwd");
?&gt;
</pre>

<p>Inside these two lines. Check they're not empty:</p>


<hr />

<pre>
<?php
print("PHP is working!\n\n");

exec("pwd", $output);
$pwd = join("",$output);
print("current directory: $pwd");
?>
</pre>

<hr />


</body>
</html>

